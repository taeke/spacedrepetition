# Spaced Repetition
Based on [The Most Powerful Way to Remember What You Study : Thomas Frank](https://www.youtube.com/watch?v=eVajQPuRmk8&t=2s) 
i created a simple console app. Written in Java. It will genereate a file questions.txt which holds 
the questions, and the progresses you make in learning the answers. It allows you to add new 
questions and will start by asking you the next question based on the times you run the app, and 
the box the question is in. You can also edit questions or delete them.

## Run
java -jar ./out/artifacts/SpacedRepetition_jar/SpacedRepetition.jar

## ToDo
- When numberOfQuestionsToAsk exceeds 40 zet remaining back to review.
- Only let questions review until numberOfQuestionsToAsk is 30.
- Let box numbers go up unlimited.
- Let 40 and 30 be flexible (read from file).

## maybe
- Change updateQuestionsAndStatistics to updateQuestionList and let it return an updated list;
- Define default Question instead of current constructor;
- Delete for review;
- Get the latest default topic and subTopic from the file;
- Show topics and number them and add to new question by number;
- Prevent #FileStates and being used in the strings;
- Escape #FileStates and #END;
- Use getter's and setters;F