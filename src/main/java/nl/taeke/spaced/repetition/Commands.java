package nl.taeke.spaced.repetition;

public enum Commands {
    ASK,
    SHOW,
    EDIT,
    ADD,
    DELETE,
    QUIT,
    SET_CORRECT,
    REVIEW_MENU,
    REVIEW_NEW,
    REVIEW_NUMBER,
    SHOW_INFORMATION,
    SET_WRONG
}
