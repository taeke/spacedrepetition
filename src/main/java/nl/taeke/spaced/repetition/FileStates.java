package nl.taeke.spaced.repetition;

public enum FileStates {
    ROUND,
    QUESTIONS,
    QUESTION_NUMBER,
    SUB_TOPIC,
    TOPIC,
    BOX,
    LAST_TIME_ASKED,
    REVIEWED,
    QUESTION_LINES,
    ANSWER_LINES
}
