package nl.taeke.spaced.repetition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Menu {
    Map<MenuStates, String> choicesPerState = new HashMap<>();
    Map<MenuStates, List<CommandLetter>> commandsPerState = new HashMap<>();
    MenuStates state = MenuStates.START;
    WindowsConsole console;
    QuestionList questionList;

    public Menu(WindowsConsole console, QuestionList questionList) {
        this.console = console;
        this.questionList = questionList;

        choicesPerState.put(MenuStates.START, "e (Edit); a (Ask); r (Review menu); i (Information); q (Quit)");
        commandsPerState.put(MenuStates.START,  new ArrayList<>());
        commandsPerState.get(MenuStates.START).add(new CommandLetter(Commands.EDIT,"E"));
        commandsPerState.get(MenuStates.START).add(new CommandLetter(Commands.ASK,"A"));
        commandsPerState.get(MenuStates.START).add(new CommandLetter(Commands.REVIEW_MENU,"R"));
        commandsPerState.get(MenuStates.START).add(new CommandLetter(Commands.SHOW_INFORMATION,"I"));
        commandsPerState.get(MenuStates.START).add(new CommandLetter(Commands.QUIT,"Q"));

        choicesPerState.put(MenuStates.REVIEW_MENU, "n (New); r (Review number); q (Quit)");
        commandsPerState.put(MenuStates.REVIEW_MENU,  new ArrayList<>());
        commandsPerState.get(MenuStates.REVIEW_MENU).add(new CommandLetter(Commands.REVIEW_NEW,"N"));
        commandsPerState.get(MenuStates.REVIEW_MENU).add(new CommandLetter(Commands.REVIEW_NUMBER,"R"));
        commandsPerState.get(MenuStates.REVIEW_MENU).add(new CommandLetter(Commands.QUIT,"Q"));

        choicesPerState.put(MenuStates.REVIEW_NEW, "e (Edit); c (Correct) d (Delete)");
        commandsPerState.put(MenuStates.REVIEW_NEW,  new ArrayList<>());
        commandsPerState.get(MenuStates.REVIEW_NEW).add(new CommandLetter(Commands.EDIT,"E"));
        commandsPerState.get(MenuStates.REVIEW_NEW).add(new CommandLetter(Commands.DELETE,"D"));
        commandsPerState.get(MenuStates.REVIEW_NEW).add(new CommandLetter(Commands.SET_CORRECT,"C"));

        choicesPerState.put(MenuStates.EDIT_FOR_REVIEW_NUMBER, "e (Edit); c (Correct) d (Delete)");
        commandsPerState.put(MenuStates.EDIT_FOR_REVIEW_NUMBER,  new ArrayList<>());
        commandsPerState.get(MenuStates.EDIT_FOR_REVIEW_NUMBER).add(new CommandLetter(Commands.EDIT,"E"));
        commandsPerState.get(MenuStates.EDIT_FOR_REVIEW_NUMBER).add(new CommandLetter(Commands.DELETE,"D"));
        commandsPerState.get(MenuStates.EDIT_FOR_REVIEW_NUMBER).add(new CommandLetter(Commands.SET_CORRECT,"C"));

        choicesPerState.put(MenuStates.SHOW_QUESTION, "s (Show answer); e (Edit); d (Delete)");
        commandsPerState.put(MenuStates.SHOW_QUESTION,  new ArrayList<>());
        commandsPerState.get(MenuStates.SHOW_QUESTION).add(new CommandLetter(Commands.SHOW,"S"));
        commandsPerState.get(MenuStates.SHOW_QUESTION).add(new CommandLetter(Commands.EDIT,"E"));
        commandsPerState.get(MenuStates.SHOW_QUESTION).add(new CommandLetter(Commands.DELETE,"D"));

        choicesPerState.put(MenuStates.SHOW_ANSWER, "c (Correct); w (Wrong); e (Edit);");
        commandsPerState.put(MenuStates.SHOW_ANSWER,  new ArrayList<>());
        commandsPerState.get(MenuStates.SHOW_ANSWER).add(new CommandLetter(Commands.SET_CORRECT,"C"));
        commandsPerState.get(MenuStates.SHOW_ANSWER).add(new CommandLetter(Commands.SET_WRONG, "W"));
        commandsPerState.get(MenuStates.SHOW_ANSWER).add(new CommandLetter(Commands.EDIT,"E"));

        choicesPerState.put(MenuStates.ADD_QUESTION, "a (Add); q (Quit);");
        commandsPerState.put(MenuStates.ADD_QUESTION,  new ArrayList<>());
        commandsPerState.get(MenuStates.ADD_QUESTION).add(new CommandLetter(Commands.ADD,"A"));
        commandsPerState.get(MenuStates.ADD_QUESTION).add(new CommandLetter(Commands.QUIT, "Q"));
    }

    public QuestionList getUpdatedQuestionList() throws IOException {
        console.clear(this.questionList.fileStatistics);
        while (this.state != MenuStates.QUIT) {
            switch (this.state) {
                case START:
                    handleStart();
                    break;
                case SHOW_QUESTION:
                    handleShowQuestion();
                    break;
                case SHOW_ANSWER:
                    handleShowAnswer();
                    break;
                case REVIEW_MENU:
                    handleReviewMenu();
                    break;
                case REVIEW_NEW:
                    handleReviewNew();
                    break;
                case ADD_QUESTION:
                    handleAddQuestion();
                    break;
                case EDIT_QUESTION:
                    this.questionList.editQuestion(QuestionIndexes.ASK);
                    this.state = MenuStates.SHOW_QUESTION;
                    break;
                case EDIT_FOR_REVIEW_NUMBER:
                    handleReviewNumber();
                    break;
                case EDIT_FOR_REVIEW_NEW:
                    this.questionList.editQuestion(QuestionIndexes.REVIEW);
                    this.state = MenuStates.REVIEW_NEW;
                    break;
                case DELETE_REVIEW_NEW:
                    this.questionList.deleteQuestion(QuestionIndexes.REVIEW);
                    this.state = MenuStates.REVIEW_NEW;
                    break;
                case DELETE_QUESTION:
                    this.questionList.deleteQuestion(QuestionIndexes.ASK);
                    this.state = MenuStates.SHOW_QUESTION;
                    break;
            }
        }

        return this.questionList;
    }

    private Commands getCommand() throws IOException {
        String letter = console.askForCommand(this.choicesPerState.get(this.state));
        for (int i = 0; i < commandsPerState.get(this.state).size(); i++) {
            if(commandsPerState.get(this.state).get(i).letter.equals(letter)) {
                return commandsPerState.get(this.state).get(i).command;
            }
        }

        console.clear(this.questionList.fileStatistics);
        console.printLn("Unknown command");
        return getCommand();
    }

    private void handleStart() throws IOException {
        switch (getCommand()) {
            case ASK:
                this.questionList.fileStatistics.round++;
                this.questionList.setCurrentAskIndex(true);
                this.state = MenuStates.SHOW_QUESTION;
                break;
            case EDIT:
                this.state = MenuStates.ADD_QUESTION;
                this.console.clear(questionList.fileStatistics);
                break;
            case REVIEW_MENU:
                this.state = MenuStates.REVIEW_MENU;
                break;
            case SHOW_INFORMATION:
                this.questionList.fileStatistics.show(console);
                this.state = MenuStates.QUIT;
                break;
            case QUIT:
                this.state = MenuStates.QUIT;
                break;
        }
    }

    private void handleShowQuestion() throws IOException {
        if(this.questionList.hasNextToAsk()) {
            this.questionList.showQuestion(QuestionIndexes.ASK);
            switch (getCommand()) {
                case SHOW:
                    this.state = MenuStates.SHOW_ANSWER;
                    break;
                case EDIT:
                    this.state = MenuStates.EDIT_QUESTION;
                    break;
                case DELETE:
                    this.state = MenuStates.DELETE_QUESTION;
                    break;
                case QUIT:
                    this.state = MenuStates.QUIT;
                    break;
            }
        } else {
            console.printLn("All questions answered");
            this.state = MenuStates.QUIT;
        }
    }

    private void handleReviewMenu() throws IOException {
        switch (getCommand()) {
            case REVIEW_NEW:
                this.questionList.setCurrentReviewNewIndex(true);
                this.state = MenuStates.REVIEW_NEW;
                break;
            case REVIEW_NUMBER:
                this.state = MenuStates.EDIT_FOR_REVIEW_NUMBER;
                break;
            case QUIT:
                this.state = MenuStates.QUIT;
                break;
        }
    }

    private void handleReviewNumber() throws IOException {
        try {
            int number = console.askForNumber("Number of the question to review.");
            if(this.questionList.hasQuestionNumber(number)) {
                this.questionList.showAnswer(number);
                switch (getCommand()) {
                    case EDIT:
                        this.questionList.editQuestion(number);
                        break;
                    case DELETE:
                        this.questionList.deleteQuestion(number);
                        break;
                    case SET_CORRECT:
                        break;
                }
            } else {
                console.printLn("Number not found");
            }

            this.state = MenuStates.REVIEW_MENU;
        } catch (NumberFormatException e) {
            console.clear(this.questionList.fileStatistics);
            console.printLn("Not a valid number: " + e.getMessage());
            handleReviewNumber();
        }
    }

    private void handleReviewNew() throws IOException {
        if(this.questionList.hasNextToReviewNew()) {
            this.questionList.showAnswer(QuestionIndexes.REVIEW);
            switch (getCommand()) {
                case EDIT:
                    this.state = MenuStates.EDIT_FOR_REVIEW_NEW;
                    break;
                case DELETE:
                    this.state = MenuStates.DELETE_REVIEW_NEW;
                    break;
                case SET_CORRECT:
                    this.questionList.updateAfterReviewNew();
                    break;
            }
        } else {
            console.printLn("All questions reviewed");
            this.state = MenuStates.QUIT;
        }
    }

    private void handleShowAnswer() throws IOException {
        this.questionList.showAnswer(QuestionIndexes.ASK);
        switch (getCommand()) {
            case EDIT:
                this.state = MenuStates.EDIT_QUESTION;
                break;
            case SET_CORRECT:
                this.questionList.updateAfterAnswering(true);
                this.state = MenuStates.SHOW_QUESTION;
                break;
            case SET_WRONG:
                this.questionList.updateAfterAnswering(false);
                this.state = MenuStates.SHOW_QUESTION;
                break;
        }
    }

    private void handleAddQuestion() throws IOException {
        switch (getCommand()) {
            case QUIT:
                this.state = MenuStates.QUIT;
                break;
            case ADD:
                this.questionList.addQuestion();
                break;
        }
    }
}
