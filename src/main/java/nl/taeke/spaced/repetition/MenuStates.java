package nl.taeke.spaced.repetition;

public enum MenuStates {
    START,
    SHOW_QUESTION,
    ADD_QUESTION,
    EDIT_QUESTION,
    DELETE_QUESTION,
    DELETE_REVIEW_NEW,
    SHOW_ANSWER,
    REVIEW_MENU,
    EDIT_FOR_REVIEW_NEW,
    EDIT_FOR_REVIEW_NUMBER,
    REVIEW_NEW,
    QUIT
}
