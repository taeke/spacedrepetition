package nl.taeke.spaced.repetition;

public class CommandLetter {
    Commands command;
    String letter;

    CommandLetter(Commands command, String letter) {
        this.command = command;
        this.letter = letter;
    }
}
