package nl.taeke.spaced.repetition;

import java.util.List;

public class Question {
    int number;
    String subTopic;
    String topic;
    List<String> questionLines;
    List<String> answerLines;
    int box;
    int lastTimeAsked;
    boolean reviewed;

    public Question(int number, String topic, String subTopic, List<String> questionLines, List<String> answerLines, int box, int lastTimeAsked, boolean reviewed) {
        this.number = number;
        this.topic = topic;
        this.subTopic = subTopic;
        this.questionLines = questionLines;
        this.answerLines = answerLines;
        this.box = box;
        this.lastTimeAsked = lastTimeAsked;
        this.reviewed = reviewed;
    }
}
