package nl.taeke.spaced.repetition;

import java.util.HashMap;
import java.util.Map;

public class FileStatistics {
    int numberOfQuestionsToAsk;
    int numberOfQuestionsToReview;
    int numberOfUnreviewedQuestions;
    Map<Integer, Integer> numberOfQuestionsPerBox;
    int totalNumberOfQuestions;
    int round;

    FileStatistics() {
        this.totalNumberOfQuestions = 0;
        this.numberOfQuestionsPerBox = new HashMap<>();
        this.numberOfQuestionsToReview = 0;
        this.numberOfQuestionsToAsk = 0;
        this.numberOfUnreviewedQuestions = 0;
        this.round = -1;
    }

    public void show(WindowsConsole console) {
        console.printLn("Total number of questions: " + totalNumberOfQuestions);
        for (int key: numberOfQuestionsPerBox.keySet()) {
            console.printLn("Number of questions in box: " + key + " is: " + numberOfQuestionsPerBox.get(key));
        }

        console.printLn("Number of unreviewed questions: " + numberOfUnreviewedQuestions);
    }
}
