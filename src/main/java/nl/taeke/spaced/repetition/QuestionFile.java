package nl.taeke.spaced.repetition;

import java.io.*;
import java.util.*;
import static java.util.stream.Collectors.toList;

public class QuestionFile {
    String fileName = "questions.txt";

    public QuestionList read(WindowsConsole console) throws FileNotFoundException {
        List<String> lines = new ArrayList<>();

        File questionFile = new File(fileName);
        if (questionFile.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;
                while ((line = br.readLine()) != null) {
                    lines.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return handleLines(console, lines);
    }

    public void save(QuestionList questionList) throws IOException {
        File file = new File(fileName);
        FileWriter fr = new FileWriter(file, false);
        fr.write("#" + FileStates.ROUND.name() + "\n");
        fr.write(questionList.fileStatistics.round + "\n");
        fr.write("#END\n");

        questionList.questions.forEach(question -> {
            try {
                this.saveQuestion(question, fr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fr.close();
    }

    private QuestionList handleLines(WindowsConsole console, List<String> lines) {
        FileStates state = FileStates.ROUND;
        List<Question> questions = new ArrayList<>();
        FileStatistics fileStatistics = new FileStatistics();
        updateQuestionsAndStatistics(lines, state, questions, fileStatistics);
        return new QuestionList(console, fileStatistics, questions);
    }

    private void updateQuestionsAndStatistics(List<String> lines, FileStates state, List<Question> questions, FileStatistics fileStatistics) {
        int index = 0;
        List<String> itemLines = new ArrayList<>();
        while (index < lines.size()) {
            String line = lines.get(index);
            if(line.equals("#END")) {
                switch (state) {
                    case ROUND:
                        fileStatistics.round = Integer.parseInt(itemLines.get(1));
                        state = FileStates.QUESTIONS;
                        break;
                    case QUESTIONS:
                        Question question = QuestionFile.linesToQuestion(itemLines);
                        updateStatistics(fileStatistics, question);
                        questions.add(question);
                        break;
                }

                itemLines = new ArrayList<>();
            } else {
                itemLines.add(line);
            }

            index++;
        }
    }

    private void updateStatistics(FileStatistics fileStatistics, Question question) {
        fileStatistics.numberOfQuestionsToAsk += (QuestionList.canShow(question, fileStatistics.round + 1) ? 1 : 0);
        fileStatistics.numberOfQuestionsToReview += (question.reviewed || fileStatistics.numberOfQuestionsToAsk + fileStatistics.numberOfQuestionsToReview >= 30 ? 0 : 1);
        fileStatistics.numberOfUnreviewedQuestions += (question.reviewed ? 0 : 1);
        if (fileStatistics.numberOfQuestionsPerBox.keySet().contains(question.box)) {
            fileStatistics.numberOfQuestionsPerBox.replace(question.box, fileStatistics.numberOfQuestionsPerBox.get(question.box) + 1);
        } else {
            fileStatistics.numberOfQuestionsPerBox.put(question.box, 1);
        }

        fileStatistics.totalNumberOfQuestions++;
    }

    private static Question linesToQuestion(List<String> itemLines) {
        Question question = new Question(0, "", "",  new ArrayList<>(),  new ArrayList<>(), 1, 0 ,false);
        FileStates state = FileStates.QUESTIONS;
        List<String> stateStrings = Arrays.stream(FileStates.values()).map(Enum::name).collect(toList());
        int index = 0;
        while(index < itemLines.size()) {
            String line = itemLines.get(index);
            if(line.startsWith("#") && stateStrings.contains(line.substring(1))) {
                state = FileStates.valueOf(line.substring(1));
            } else {
                handleLine(question, state, line);
            }

            index++;
        }

        return question;
    }

    private static void handleLine(Question question, FileStates state, String line) {
        switch (state) {
            case QUESTION_NUMBER:
                question.number = Integer.parseInt(line);
                break;
            case SUB_TOPIC:
                question.subTopic = line;
                break;
            case TOPIC:
                question.topic = line;
                break;
            case BOX:
                question.box = Integer.parseInt(line);
                break;
            case LAST_TIME_ASKED:
                question.lastTimeAsked = Integer.parseInt(line);
                break;
            case REVIEWED:
                question.reviewed = Boolean.parseBoolean(line);
                break;
            case QUESTION_LINES:
                question.questionLines.add(line);
                break;
            case ANSWER_LINES:
                question.answerLines.add(line);
                break;
        }
    }

    private void saveQuestion(Question question, FileWriter fr) throws IOException {
        fr.write("#" + FileStates.QUESTION_NUMBER + "\n");
        fr.write(question.number + "\n");
        fr.write("#" + FileStates.SUB_TOPIC + "\n");
        fr.write(question.subTopic + "\n");
        fr.write("#" + FileStates.TOPIC + "\n");
        fr.write(question.topic + "\n");
        fr.write("#" + FileStates.BOX + "\n");
        fr.write(question.box + "\n");
        fr.write("#" + FileStates.LAST_TIME_ASKED + "\n");
        fr.write(question.lastTimeAsked + "\n");
        fr.write("#" + FileStates.REVIEWED + "\n");
        fr.write(question.reviewed + "\n");
        fr.write("#" + FileStates.QUESTION_LINES + "\n");

        question.questionLines.forEach(line -> {
            try {
                fr.write(line + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fr.write("#" + FileStates.ANSWER_LINES + "\n");
        question.answerLines.forEach(line -> {
            try {
                fr.write(line + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fr.write("#END\n");
    }
}
