package nl.taeke.spaced.repetition;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        WindowsConsole console = new WindowsConsole();
        QuestionFile questionFile = new QuestionFile();
        QuestionList questionList = questionFile.read(console);
        Menu menu = new Menu(console, questionList);
        questionFile.save(menu.getUpdatedQuestionList());
    }
}
