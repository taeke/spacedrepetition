package nl.taeke.spaced.repetition;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class WindowsConsole {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void line() {
        System.out.println(String.join("", Collections.nCopies(120, "_")));
    }

    public void printLn(String message) {
        System.out.println(message);
    }

    public void printLnMulti(List<String> lines) {
        lines.forEach(System.out::println);
    }

    public String askForCommand(String question) throws IOException {
        System.out.println(question);
        return reader.readLine().toUpperCase(Locale.ROOT);
    }

    public int askForNumber(String question) throws IOException {
        System.out.println(question);
        return Integer.parseInt(reader.readLine());
    }

    public String read(String field, String defaultValue) throws IOException {
        System.out.println("New " + field + (defaultValue.equals("")? ":" : " or press enter for current/default value:"));
        String newValue = reader.readLine();
        return newValue.equals("") ? defaultValue : newValue;
    }

    public List<String> readMulti(String field, List<String> defaultValue) throws IOException {
        System.out.println("New " + field + " add lines and close with #END" + (defaultValue.isEmpty() ? ":" :" or press enter for current/default value:"));
        String name = reader.readLine();
        List<String> result = new ArrayList<>();
        while (!name.equals("#END") && !(name.equals("") && result.isEmpty())) {
            result.add(name);
            name = reader.readLine();
        }

        return result.isEmpty() ? defaultValue : result;
    }

    public void clear(FileStatistics fileStatistics) {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            String numberOfQuestionsToAsk = "Number of questions to ask: " + fileStatistics.numberOfQuestionsToAsk + ".   ";
            String numberOfQuestionsToReview = "Number of questions to review: " + fileStatistics.numberOfQuestionsToReview + ".";
            String round = "Round: " + fileStatistics.round  + ".   ";
            System.out.println(round + numberOfQuestionsToAsk  + numberOfQuestionsToReview);
            line();
        } catch (IOException | InterruptedException ignored) {
        }
    }
}
