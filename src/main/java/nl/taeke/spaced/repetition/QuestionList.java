package nl.taeke.spaced.repetition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QuestionList {
    WindowsConsole console;
    List<Question> questions;
    String lastTopic = "";
    String lastSubTopic = "";

    int currentAskIndex = -1;
    int currentReviewNewIndex = -1;
    FileStatistics fileStatistics;

    QuestionList(WindowsConsole console, FileStatistics fileStatistics, List<Question> questions) {
        this.console = console;
        this.fileStatistics = fileStatistics;
        this.questions = questions;
    }

    private void showTopic(Question question) {
        this.console.clear(fileStatistics);
        console.printLn("Question number: " + question.number + ". Box: " + question.box + ". Topic: " + question.topic);
    }

    private void showSubTopic(Question question) {
        this.showTopic(question);
        console.printLn("Sub topic: " + question.subTopic);
        console.line();
    }

    public void showQuestion(QuestionIndexes questionIndexes) {
        Question question = getQuestion(questionIndexes);
        this.showQuestion(question);
    }

    private void showQuestion(Question question) {
        this.showSubTopic(question);
        console.printLnMulti(question.questionLines);
        console.line();
    }

    public void showAnswer(int number) {
        Question question = getQuestion(number);
        this.showAnswer(question);
    }

    public void showAnswer(QuestionIndexes questionIndexes) {
        Question question = getQuestion(questionIndexes);
        this.showAnswer(question);
    }

    private void showAnswer(Question question) {
        this.showQuestion(question);
        console.printLnMulti(question.answerLines);
        console.line();
    }

    public boolean hasQuestionNumber(int number) {
        for(int i = 0; i < this.questions.size(); i++) {
            if(this.questions.get(i).number == number) {
                return true;
            }
        }

        return false;
    }

    private int getQuestionIndex(int number) {
        for(int i = 0; i < this.questions.size(); i++) {
            if(this.questions.get(i).number == number) {
                return i;
            }
        }

        throw new IllegalArgumentException("Number : " + number + " is unknown in questionList.");
    }

    private Question getQuestion(int number) {
        for(int i = 0; i < this.questions.size(); i++) {
            if(this.questions.get(i).number == number) {
                return this.questions.get(i);
            }
        }

        throw new IllegalArgumentException("Number : " + number + " is unknown in questionList.");
    }

    private Question getQuestion(QuestionIndexes questionIndexes) {
        Question question;
        switch (questionIndexes) {
            case ASK:
                question = this.questions.get(this.currentAskIndex);
                break;
            case REVIEW:
                question =  this.questions.get(this.currentReviewNewIndex);
                break;
            default: question =this.questions.get(this.currentAskIndex);
        }

        return question;
    }

    public void updateAfterReviewNew() {
        this.questions.get(this.currentReviewNewIndex).reviewed = true;

        setCurrentReviewNewIndex(true);
        fileStatistics.numberOfQuestionsToReview--;
        fileStatistics.numberOfQuestionsToAsk++;
    }

    public void updateAfterAnswering(boolean isCorrect) {
        Question question = this.questions.get(this.currentAskIndex);
        if (isCorrect) {
            question.box++;
        } else {
            question.box = 1;
        }

        this.questions.get(this.currentAskIndex).lastTimeAsked = this.fileStatistics.round;

        setCurrentAskIndex(true);
        fileStatistics.numberOfQuestionsToAsk--;
    }

    public void setCurrentAskIndex(boolean setNext) {
        if (setNext) this.currentAskIndex++;
        while (this.currentAskIndex < this.questions.size() && !QuestionList.canShow(this.questions.get(this.currentAskIndex), this.fileStatistics.round)) {
            this.currentAskIndex++;
        }
    }

    public void setCurrentReviewNewIndex(boolean setNext) {
        if (setNext) this.currentReviewNewIndex++;
        while (this.currentReviewNewIndex < this.questions.size() && questions.get(this.currentReviewNewIndex).reviewed) {
            this.currentReviewNewIndex++;
        }
    }

    static public boolean canShow(Question question, int round) {
        boolean show = false;
        if (question.reviewed) {
            if(question.box == 1) {
                show = true;
            } else {
                if (round == question.lastTimeAsked + Math.pow(2, question.box -1)) show = true;
            }
        }

        return show;
    }

    public boolean hasNextToAsk() {
        return this.currentAskIndex < this.questions.size();
    }

    public boolean hasNextToReviewNew() {
        return this.currentReviewNewIndex < this.questions.size() && fileStatistics.numberOfQuestionsToAsk < 30;
    }

    public void deleteQuestion(int number) {
        this.questions.remove(this.getQuestionIndex(number));
    }

    public void deleteQuestion(QuestionIndexes questionIndexes) {
        switch (questionIndexes) {
            case ASK:
                this.questions.remove(this.currentAskIndex);
                this.setCurrentAskIndex(false);
                fileStatistics.numberOfQuestionsToAsk--;
                break;
            case REVIEW:
                this.questions.remove(this.currentReviewNewIndex);
                this.setCurrentReviewNewIndex(false);
                fileStatistics.numberOfQuestionsToReview--;
                break;
        }

    }

    public void editQuestion(int number) throws IOException {
        updateQuestion(this.getQuestion(number));
    }

    public void editQuestion(QuestionIndexes questionIndexes) throws IOException {
        updateQuestion(this.getQuestion(questionIndexes));
    }

    private Question updateQuestion(Question question) throws IOException {
        this.showTopic(question);
        console.line();
        question.topic = console.read("topic", question.topic);
        if (question.subTopic.isEmpty()) {
            this.showTopic(question);
            console.line();
        } else {
            this.showSubTopic(question);
        }

        question.subTopic = console.read("sub topic", question.subTopic);
        if (question.questionLines.isEmpty()) {
            this.showSubTopic(question);
        } else {
            this.showQuestion(question);
        }

        this.lastTopic = question.topic;
        this.lastSubTopic = question.subTopic;
        question.questionLines = console.readMulti("question", question.questionLines);
        if (question.answerLines.isEmpty()) {
            this.showQuestion(question);
        } else {
            this.showAnswer(question);
        }

        question.answerLines = console.readMulti("answer", question.answerLines);
        this.console.clear(fileStatistics);
        return question;
    }

    public void addQuestion() throws IOException {
        int number = this.questions
                .stream()
                .mapToInt(q -> q.number)
                .filter(n -> n >= 0)
                .max()
                .orElse(0);

        Question question = new Question(number + 1, lastTopic, lastSubTopic, new ArrayList<>(), new ArrayList<>(), 1, this.fileStatistics.round, false);
        this.questions.add(this.updateQuestion(question));
        fileStatistics.numberOfQuestionsToReview++;
        this.console.clear(fileStatistics);
    }
}
